# Role `hostname`

Manage *hostname* and records of *self* in `/etc/hosts` file.


## Variables

| Variable      | Required | Default                    | Description                                                 |
|---------------|----------|----------------------------|-------------------------------------------------------------|
| hostname      | no       | `inventory_hostname_short` | Desired machine *hostname*                                  |
|               |          |                            |                                                             |
| ipv4_address  | no       |                            | (Static) IPv4 address for `hostname` record in `hosts` file |
| ipv6_address  | no       |                            | (Static) IPv6 address for `hostname` record in `hosts` file |
|               |          |                            |                                                             |
| extra_hosts   | no       |                            | List of extra records in `/etc/hosts`:                      |
| .hostname     | yes      |                            | Hostname of the extra hosts record                          |
| .ipv4_address | no       |                            | IPv4 for the extra hosts record                             |
| .ipv6_address | no       |                            | IPv6 for the extra hosts record                             |
