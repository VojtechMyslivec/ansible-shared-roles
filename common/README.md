# Role `common`

Install and configure basic tools on Debian server.


## Description

The role will install `vim`, `bash-completion`, `htop` and similar packages and
configure *vim* and *bash* to increase CLI user experience.


## Variables

| Variable              | Required | Default | Description                                       |
|-----------------------|----------|---------|---------------------------------------------------|
| common_extra_packages | no       |         | List of extra apt packages to install on the host |
