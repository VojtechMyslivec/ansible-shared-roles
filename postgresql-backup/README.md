# Role `postgresql-backup`

Deploy simple PostgreSQL database backups.


## Description

Simple backups with `pg_dump` are created on regular basis and there are
24 *hourly* dumps, 7 *daily* dumps and 4 *weekly* dumps kept.

Each series can be switched off by appropriate flag in variables.


## Variables

| Variable                  | Required  | Default               | Description |
| ------------------------- | --------- | --------------------- | ----------- |
| postgresql_backup_dir     | no        | /var/backup/postgres  | Directory to store hourly, daily and weekly backups |
| postgresql_backup_user    | no        | postgres              | System user with database admin/dump privileges |
|                           |           |                       |  |
| postgresql_backup         | no        |                       | Dictionary with backup parameters |
| .hourly                   | no        | true                  | Enable and keep hourly backups |
| .hourly_count             | no        | 24                    | Keep that many hourly backups |
| .daily                    | no        | true                  | Enable and keep daily backups |
| .daily_count              | no        | 7                     | Keep that many daily backups |
| .weekly                   | no        | true                  | Enable and keep weekly backups |
| .weekly_count             | no        | 4                     | Keep that many weekly backups |
