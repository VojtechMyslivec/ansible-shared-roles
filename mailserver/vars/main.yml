---
# Ansible vars
#
# mailserver configuration parameters
#
# These variables are in this separate file mainly for arrangement – to not
# bother in defaults or tasks files. Variables serve mainly just as list for
# key-value configuration parameters matching.
# If any value needs to be changed, `mail_*` variable should be used and
# defined in defaults

mailserver_ciphers: '{{ (mail_tls_ciphers + ["@STRENGTH"]) | join(":") }}'


mailserver_opendkim_config:
  - name: 'Socket'
    value: 'local:/var/spool/postfix/opendkim/opendkim.sock'

  - name: 'Domain'
    value: '{{ mail_dkim_domains | join(",") }}'

  - name: 'KeyFile'
    value: '/etc/dkimkeys/{{ mail_dkim_selector }}.private'

  - name: 'Selector'
    value: '{{ mail_dkim_selector }}'

  - name: 'InternalHosts'
    value: 'refile:/etc/opendkim.hosts'


mailserver_postfix_config:
  - name: 'myorigin'
    value: '/etc/mailname'

  - name: 'myhostname'
    value: '{{ mail_name }}'

  - name: 'mydestination'
    value: '$myhostname, {{ inventory_hostname_short }}, {{ inventory_hostname }}, localhost.localdomain, localhost'

  - name: 'smtpd_relay_restrictions'
    value: >
      permit_mynetworks,
      reject_non_fqdn_recipient,
      reject_unknown_recipient_domain,
      permit_sasl_authenticated,
      defer_unauth_destination

  - name: 'smtpd_sender_restrictions'
    value: >
      permit_mynetworks,
      reject_non_fqdn_sender

  - name: 'compatibility_level'
    value: '3.6'

  - name: 'recipient_delimiter'
    value: '+'

  - name: 'append_dot_mydomain'
    value: 'no'

  - name: 'disable_vrfy_command'
    value: 'yes'

  - name: 'smtpd_helo_required'
    value: 'yes'

  - name: 'smtpd_sasl_auth_enable'
    value: 'no'

  - name: 'smtpd_tls_auth_only'
    value: 'yes'

  - name: 'smtpd_tls_received_header'
    value: 'yes'

  - name: 'smtpd_use_tls'
    value: ''
    state: absent

  - name: 'smtpd_tls_security_level'
    value: '{{ "may" if mail_smtp_starttls_enabled else "none" }}'

  - name: 'smtpd_tls_key_file'
    value: '{{ mail_tls_key }}'

  - name: 'smtpd_tls_cert_file'
    value: '{{ mail_tls_cert }}'

  - name: 'smtpd_tls_session_cache_database'
    value: 'btree:${data_directory}/smtpd_scache'

  - name: 'smtp_tls_session_cache_database'
    value: 'btree:${data_directory}/smtp_scache'

  - name: 'smtp_tls_security_level'
    value: '{{ "encrypt" if mail_relayhost != "" else "dane" }}'

  - name: 'smtp_dns_support_level'
    value: 'dnssec'

  - name: 'mynetworks'
    value: '{{ (["127.0.0.0/8", "[::ffff:127.0.0.0]/104", "[::1]/128"] + mail_mynetworks) | join(", ") }}'

  - name: 'relay_domains'
    value: '{{ mail_relay_domains | join(", ") }}'

  - name: 'message_size_limit'
    value: '{{ mail_message_size_limit * 1024 * 1024 }}'

  - name: 'mailbox_size_limit'
    value: '{{ mail_mailbox_size_limit * 1024 * 1024 }}'


mailserver_postfix_relayhost_config:
  - name: relayhost
    value: '[{{ mail_relayhost }}]:{{ mail_relayhost_port }}'

  - name: smtp_tls_wrappermode
    value: '{{ "yes" if mail_relayhost_use_smtps else "no" }}'

  - name: 'smtp_sasl_auth_enable'
    value: '{{ "yes" if mail_relayhost_credentials != "" else "no" }}'

  - name: 'smtp_sasl_security_options'
    value: 'noanonymous'

  - name: 'smtp_sasl_password_maps'
    value: '{{ "hash:/etc/postfix/sasl_passwd" if mail_relayhost_credentials != "" else "" }}'


mailserver_postfix_opendkim_config:
  - name: 'milter_protocol'
    value: '2'

  - name: 'milter_default_action'
    value: 'accept'

  - name: 'smtpd_milters'
    value: 'unix:opendkim/opendkim.sock'

  - name: 'non_smtpd_milters'
    value: '$smtpd_milters'


mailserver_postfix_auth_config:
  - name: 'smtpd_sasl_type'
    value: 'dovecot'

  - name: 'smtpd_sasl_path'
    value: 'private/auth'


mailserver_postfix_delivery_config:
  - name: 'virtual_mailbox_domains'
    value: '{{ mail_domains | join(", ") }}'

  - name: 'virtual_alias_maps'
    value: 'hash:/etc/postfix/virtual'

  - name: 'virtual_mailbox_base'
    value: '{{ mail_virtual_base }}'

  - name: 'virtual_mailbox_maps'
    value: 'hash:/etc/postfix/vmailbox'

  - name: 'virtual_uid_maps'
    value: 'static:{{ mail_vmail_user_id }}'

  - name: 'virtual_gid_maps'
    value: 'static:{{ mail_vmail_user_id }}'

  - name: 'delay_warning_time'
    value: '{{ mail_delay_warning_time }}'


mailserver_postfix_auth_filter_config:
  - name: 'smtpd_sender_login_maps'
    value: 'hash:/etc/postfix/sender_login_map'


mailserver_smtp_master_config: |
  # service type private unpriv chroot wakeup maxproc command + args
  {% if mail_instances is not defined %}
  smtp inet n - y - - smtpd
  {% else %}
  {%   for instance in mail_instances %}
  {{ instance.listen_address }}:smtp inet n - y - - smtpd
  {%     if instance.config is defined %}
  {%       for name, value in instance.config.items() %}
    -o {{ name }}={{ value }}
  {%       endfor %}
  {%     endif %}
  {%   endfor %}
  {% endif %}


mailserver_submissions_master_config: |
  # service type private unpriv chroot wakeup maxproc command + args
  smtps inet n - y - - smtpd
    -o syslog_name=postfix/smtps
    -o smtpd_tls_wrappermode=yes
    {% if mail_tls1_enabled -%}
    -o smtpd_tls_mandatory_protocols=!SSLv2,!SSLv3
    {% else -%}
    -o smtpd_tls_mandatory_protocols=!SSLv2,!SSLv3,!TLSv1,!TLSv1.1
    {% endif -%}
    -o smtpd_tls_mandatory_ciphers=high
    -o tls_high_cipherlist={{ mailserver_ciphers }}
    {% if mail_auth_enabled -%}
    -o smtpd_sasl_auth_enable=yes
    {% endif -%}
    {% if mail_restrict_submission_sender and mail_auth_filter_enabled -%}
    -o { smtpd_sender_restrictions = reject_authenticated_sender_login_mismatch, check_sender_access hash:/etc/postfix/access, reject }
    {% elif mail_restrict_submission_sender -%}
    -o { smtpd_sender_restrictions = check_sender_access hash:/etc/postfix/access, reject }
    {% elif mail_auth_filter_enabled -%}
    -o { smtpd_sender_restrictions = reject_authenticated_sender_login_mismatch, permit }
    {% endif -%}
    {% if mail_limit_enabled -%}
    -o { smtpd_data_restrictions = check_policy_service inet:127.0.0.1:10040, permit }
    {% endif -%}


mailserver_submission_master_config: |
  # service type private unpriv chroot wakeup maxproc command + args
  submission inet n - y - - smtpd
    -o syslog_name=postfix/submission
    -o smtpd_tls_security_level=encrypt
    {% if mail_tls1_enabled -%}
    -o smtpd_tls_mandatory_protocols=!SSLv2,!SSLv3
    {% else -%}
    -o smtpd_tls_mandatory_protocols=!SSLv2,!SSLv3,!TLSv1,!TLSv1.1
    {% endif -%}
    -o smtpd_tls_mandatory_ciphers=high
    -o tls_high_cipherlist={{ mailserver_ciphers }}
    {% if mail_auth_enabled -%}
    -o smtpd_sasl_auth_enable=yes
    {% endif -%}
    {% if mail_restrict_submission_sender and mail_auth_filter_enabled -%}
    -o { smtpd_sender_restrictions = reject_authenticated_sender_login_mismatch, check_sender_access hash:/etc/postfix/access, reject }
    {% elif mail_restrict_submission_sender -%}
    -o { smtpd_sender_restrictions = check_sender_access hash:/etc/postfix/access, reject }
    {% elif mail_auth_filter_enabled -%}
    -o { smtpd_sender_restrictions = reject_authenticated_sender_login_mismatch, permit }
    {% endif -%}
    {% if mail_limit_enabled -%}
    -o { smtpd_data_restrictions = check_policy_service inet:localhost:10040, permit }
    {% endif -%}

...
