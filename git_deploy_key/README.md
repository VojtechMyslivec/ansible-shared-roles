# Role `git_deploy_key`

Generate ssh key and add git server among known hosts.


## Variables

| Variable              | Required | Default    | Description                                          |
|-----------------------|----------|------------|------------------------------------------------------|
| git_deploy_key_user   | no       | root       | Generate ssh key for this user                       |
| git_deploy_key_type   | no       | ed25519    | Type of the ssh key to use                           |
| git_deploy_key_bits   | no       |            | Specify key lenght (each type has different options) |
|                       |          |            |                                                      |
| git_deploy_key_server | no       | gitlab.com | Git server host name to add among known hosts        |
