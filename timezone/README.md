# Role `timezone`

Role to set server *timezone* via *timezone* module. *cron* service is
restarted if the timezone changed.


## Variables

| Variable | Required | Default | Description                   |
|----------|----------|---------|-------------------------------|
| timezone | no       | Etc/UTC | Name of the server *timezone* |
