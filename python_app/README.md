# Role `python_app`

Run a Python application from virtual environment as a system service.


## Description

Prepare Python environment – including dedicated system user, file system
structure, *systemd* service, virtual environment and required packages – and
run the app as a system service.


## Variables

| Variable                 | Required | Default           | Description                                                                 |
|--------------------------|----------|-------------------|-----------------------------------------------------------------------------|
| upgrade                  | no       | false             | Perform tasks to upgrade Python app                                         |
| purge_virtualenv         | no       | false             | Remove (and reinstall) Python virtual environment                           |
|                          |          |                   |                                                                             |
| python_app_name          | yes      |                   | System-wide unique system identifier (used for user names, file names etc.) |
|                          |          |                   |                                                                             |
| python_app_repo_url      | yes      |                   | Git URL of a repo with source code to deploy                                |
| python_app_repo_version  | no       | main              | Git revision of fetched repository                                          |
| python_app_repo_key_file | no       | ~/.ssh/git_deploy | Path to a ssh key file to use to fetch git repository                       |
|                          |          |                   |                                                                             |
| python_app_requirements  | no       |                   | Extra apt packages to install                                               |
| python_app_packages      | no       |                   | Extra pip packages to install                                               |
|                          |          |                   |                                                                             |
| python_app_executable    | yes      |                   | Path  to a Python script in the repository to execute as a service          |
| python_app_args          | no       |                   | List of arguments to the script to execute                                  |
| python_app_env           | no       |                   | A dictionary of (private) environment variables for the service             |


## Example

Deploy simple Python app:

```yaml
python_app_name: 'simple'
python_app_executable: 'main.py'
python_app_repo_url: 'git@gitlab.com/example/python.git'
```


Install some packages from PyPI:

```yaml
python_app_packages:
  - requests
  - PyYAML
```


Define some environment variables:

```yaml
python_app_env:
  secret_parameter: 'H1d3M3W3ll'
```
