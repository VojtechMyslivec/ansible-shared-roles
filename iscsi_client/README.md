# Role `iscsi_client`

Install *iSCSI* tools and configure defined *iSCSI* target.


## Variables

| Variable             | Required | Default | Description                              |
|----------------------|----------|---------|------------------------------------------|
| iscsi_portal         | yes      |         | Name of the iSCSI host to scan           |
| iscsi_initiator_name | yes      |         | Name of the iSCSI initiator              |
|                      |          |         |                                          |
| iscsi_chap_auth      | yes      |         | Configure CHAP authentication if defined |
| .username            | yes      |         | CHAP username                            |
| .password            | yes      |         | CHAP password                            |
