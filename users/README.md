# Role `users`

The role will create OS users.


## Description

Each user must have its ssh key, password file and comment file in the `users`
subdirectory within ansible `files`. `NAME.ssh` is public ssh keys as in
`authorized_keys` file, `NAME.passwd` is user' password hash in form of the
password field in the `/etc/shadow` (should be encrypted with *ansible-vault*)
and `NAME.comment` contains one line of arbitrary string that should hold the
full name of a user (i.e. the *GECOS* field).


### Password format

The password file can be generated via:

```
mkpasswd -m sha-512 -R 99999 > files/users/johndoe.passwd
```


### Restricted users

Users restricted to *port-forwarding* only can be created by specifying
`restricted_port_forwarding` list of allowed destination for them. These users
will not be able to request TTY on a server, run commands or use any other
forwarding.

If this parameter is not set, regular user can use port-forwarding without
limits.

Format of the `host` and `port` parameters comes from `permitopen` option
format in *AUTHORIZED_KEYS FILE FORMAT* section in `sshd(8)`:

> IPv6 addresses can be specified by enclosing the address in square brackets.
> No pattern matching is performed on the specified *hostnames*, they must be
> literal domains or addresses. A port specification of * matches any port.

*Note*: Setting a `sudo` permissions to restricted users does not make sense as
they are not able to run any commands.


## Variables

| Variable                    | Required | Default | Description                                                                                |
|-----------------------------|----------|---------|--------------------------------------------------------------------------------------------|
| users_default_groups        | no       |         | List of secondary groups of each user in `users` and `default_users`                       |
| users_force_password        | no       | true    | Force (reset) given user password; if not, user password will be set only on user creation |
|                             |          |         |                                                                                            |
| default_users               | no       |         | List of default server users; Same as `users`                                              |
| users                       | no       |         | List of extra users; Each item is a dictionary with folowing parameters                    |
| .name                       | yes      |         | Name of the user and primary group                                                         |
| .state                      | no       | present | Remove the user is set to `absent`                                                         |
| .groups                     | no       |         | List of extra secondary groups                                                             |
| .sudo                       | no       | false   | Add `ALL` *sudo* privileges                                                                |
| .sudo_users                 | no       |         | Allow to run *sudo* commands as given list of users                                        |
| .nopasswd                   | no       | false   | Require password to run `sudo`                                                             |
| .restricted_port_forwarding | no       |         | List of allowed destinations for port-forwarding-only restricted user                      |
| ..host                      | yes      |         | Allowed destination host                                                                   |
| ..port                      | yes      |         | Allowed destination port                                                                   |

It is intended to configure `default_users` on group level and `users` to add
custom users per *host*.


## Examples

`group_vars/all.yml`

```yaml
users_default_groups:
  - 'ssh'

default_users:
  - name: 'neo'
    sudo: true
    nopasswd: true
```

`files/users/neo.comment`:

```
Thomas Anderson
```


`host_vars/zion.matrix.org.yml`

```yaml
users:
  - name: 'morpheus'
    groups:
      - 'captains'
```

`files/users/morpheus.comment`:

```
Captain of Nebuchadnezzar
```


`tree files/users`:

```
files/users/
├── morpheus.comment
├── morpheus.passwd
├── morpheus.ssh
├── neo.comment
├── neo.passwd
└── neo.ssh
```


Port-forwarding only user with access to ssh on some other server:

```yaml
users:
  - name: 'trinity'
    restricted_port_forwarding:
      - host: 'rrf-control.matrix.net'
        port: 22
```

`files/users/trinity.comment`:

```
Trinity should stay out of The Matrix
```
