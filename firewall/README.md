# Role `firewall`

Configure *iptables* and *fail2ban* rules and service on Debian server.


## Description

Role will install `iptables-persistent` service to manage firewall and set
configuration files for *IPv4* and *IPv6* rules.

*Default policies* of firewall is set to `ACCEPT` for *outgoing* traffic,
`REJECT` for *incoming* and `DROP` for *forwarding* (can be set to `ACCEPT`).

Following traffic is accepted in `INPUT` chain:
- *Related* and *established* packets
- *loopback* interface
- *icmp* protocol
- *link-local* and *multicast* addresses (IPv6 only)
- Exceptions set by variables below

There is special named *chain* for every network in the infrastructure and they
are described in the following table (networks might overlap):

| Chain    | Description                        | Networks                  |
| -------- | ---------------------------------- | ------------------------- |
| incoming | Whole Internet                     | 0.0.0.0/0                 |
| special  | For per-ip rules                   | –                         |


## Variables

| Variable              | Required | Default              | Description                                                              |
|-----------------------|----------|----------------------|--------------------------------------------------------------------------|
| firewall_skip_restart | no       | false                | Skip firewall and fail2ban services restart (usefull for Docker hosts)   |
| firewall_service      | no       | netfilter-persistent | Name of the firewall service; use `iptables-persistent` on older servers |
|                       |          |                      |                                                                          |
| fail2ban_install      | no       | true                 | Install and configure `fail2ban`                                         |
| fail2ban_ignore_hosts | no       |                      | List of ignored hosts, IPs and subnets (whitelist)                       |

The table below describes variables in variants due to chain name (above) a due
to transport protocol (*UDP*/*TCP*). Following placeholders must be replaced by
desired variants:
- `XXXX`: Name of the chain in the table above
- `YYY`: `udp` or `tcp`

For example: `XXXX_YYY` is replaced by `special_tcp`, `incoming_udp` or so.

All parameters are in `firewall` dictionary.

| Variable        | Required | Default | Description                                                               |
|-----------------|----------|---------|---------------------------------------------------------------------------|
| enable_forward  | no       | false   | Set *default policy* for `FORWARD` to `ACCEPT`                            |
|                 |          |         |                                                                           |
| enable_ssh      | no       | true    | Allow *ssh* access from everywhere                                        |
| enable_mdns     | no       | false   | Allow *mDNS* communication on multicast address from everywhere           |
|                 |          |         |                                                                           |
| XXXX_YYY        | no       |         | List of open *TCP*/*UDP* ports in `XXXX` chain                            |
|                 |          |         |                                                                           |
| special_YYY     | no       |         | Dictionary for per-ip rules; Each item has following parameters:          |
| .ip             | no       |         | Source *IPv4* address (for v4 rules)                                      |
| .ip6            | no       |         | Source *IPv6* address (for v6 rules)                                      |
| .port           | yes      |         | Open *TCP*/*UDP* port                                                     |
|                 |          |         |                                                                           |
| custom          | no       |         | List of custom *iptables* rules (including action `-j ACCEPT` and so on)  |
| custom6         | no       |         | List of custom *ip6tables* rules (including action `-j ACCEPT` and so on) |

Every host has its IP address in `ipv4_address` and/or `ipv6_address`
variables. It is **strongly recommended** to use this variable in firewall
rules definition.

For example: `hostvars["dan.gallowmere.net"].ipv4_address` instead of `172.16.65.42`.


### Examples

Enable mDNS service for a desktop computer:

```yaml
firewall:
  enable_mdns: true
```


Enable http(s) from everywhere:

```yaml
firewall:
  incoming_tcp:
    - 80
    - 443
```


Define per-ip rules:

```yaml
firewall:
  incoming_tcp:
    - 80
    - 443

  special_tcp:
    - ip6: '{{ hostvars["backup.example.org"].ipv6_address }}'
      port: 22
```


Allow *forwarding* (i.e. for hypervisor):

```yaml
firewall:
  enable_forward: true
```


Allow ssh *only* from specific IP:

```yaml
firewall:
  enable_ssh: false
  special_tcp:
    - ip: '203.0.113.42'
      ip6: '2001:db8::dead:beef:42'
      port: 22
```


Add custom rule to enable *VRRP* protocol:

```yaml
firewall:
  custom:
    - '-d 224.0.0.18 -p vrrp -j ACCEPT'
```
