---
# Ansible role
#
# Install and setup PostgreSQL db server

- block:
  - name: Add apt key for pgdg
    apt_key:
      url: '{{ pg_key_url }}'
      state: present

  - name: Add pgdg repository
    apt_repository:
      repo: '{{ pg_repo }}'
      state: present
      filename: 'postgresql'

  - name: Install PostgreSQL
    apt:
      name: '{{ pg_requirements }}'
      state: present

  tags:
    - apt


- block:
  - name: Configure postgresql parameters (restart needed)
    lineinfile:
      path: '{{ pg_config_file }}'
      regexp: '^\s*{{ item.key }}\s*='
      line: '{{ item.key }} = {{ item.value }}'
    notify: Restart postgresql
    loop: '{{ pg_config_restart_options }}'

  - name: Configure postgresql parameters
    lineinfile:
      path: '{{ pg_config_file }}'
      regexp: '^\s*{{ item.key }}\s*='
      line: '{{ item.key }} = {{ item.value }}'
    notify: Reload postgresql
    loop: '{{ pg_config_options }}'

  - name: Configure pg_hba
    template:
      src: 'pg_hba.conf.j2'
      dest: '{{ pg_hba_file }}'
      owner: 'postgres'
      group: 'postgres'
      mode: '0640'
    notify: Reload postgresql
    tags:
      - pg_hba
      - db_databases
      - db_users
      - db_replication

  - name: Configure psqlrc
    copy:
      src: 'psqlrc.conf'
      dest: '/etc/postgresql-common/psqlrc'
      owner: 'root'
      group: 'root'
      mode: '0644'

  tags:
    - config


- block:
  - name: Configure databases
    include_tasks: 'db_databases.yml'
    tags:
      - db_databases

  - name: Configure extra db users
    include_tasks: 'db_users.yml'
    loop: '{{ pg_users|dict2items }}'
    loop_control:
      loop_var: pg_user
      label: '{{ pg_user.key }}'
    tags:
      - db_users

  - name: Configure database extensions
    include_tasks: 'db_extensions.yml'
    tags:
      - db_extensions

  when: not pg_replication_enabled or inventory_hostname == pg_replication_master


- name: Configure database replication
  include_tasks: 'db_replication.yml'
  when: pg_replication_enabled
  tags:
    - db_replication

...
