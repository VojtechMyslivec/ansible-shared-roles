# Role `postgresql`

Install and setup PostgreSQL DB on Debian server.


## Description

This role installs PostgreSQL version 13 from official PostgreSQL Global
Development Group repositories. Default parameters are set to deploy sane and
usable database config, however *tuning* parameters (below) should be set to
reflect particular hardware capacities.

Security aspects were also bear in mind, e.g. `scram-sha-256` for password
hashing is used by default although it is not compatible with older (<10)
clients. Managed databases have restricted access solely to its owner and
optionally to defined extra users.

PostgreSQL host-based-access application firewall (`pg_hba`) is managed so
remote users can connect solely to their databases.

Must-have extension `pg_stat_statements` is installed and enabled in
all managed databases.


## Variables

### Database and users definition

PostgreSQL databases are defined in `pg_databases` dictionary. Each item name
serves as a *database name* and values are following parameters:

| Variable        | Required  | Default   | Description |
| --------------- | --------- | --------- | ----------- |
| name            | yes       |           | Name of the created database |
| user            | no        | `name`    | Name of the database owner; The user is created if does not exist |
| password        | no        |           | Password for the user; If not set, user can log in only based on local `peer` method (i.e. verified local system user) |
| remote_hosts    | no        |           | List of remote addresses from where the owner is allowed to connect to the database |


Extra users (others than db owners) can be defined in `pg_users` dictionary.
Each item name serves as a *user name* and values are following parameters:

| Variable        | Required  | Default   | Description |
| --------------- | --------- | --------- | ----------- |
| name            | yes       |           | Name of the database user |
| state           | no        | present   | Whether to `ADD` or `DROP` the user (and his/her privileges) |
| password        | no        |           | User password |
| privileges      | no        |           | List of user privileges; Item parameters are described in `postgresql_privs` module with defaults below: |
| .database       | yes       |           | DB to apply privileges to |
| .schema         | no        |           | Schema that contains the database objects specified via `objs` |
| .privs          | no        |           | Comma separated list of privileges |
| .type           | no        | database  | Database object type to apply the privileges on |
| .objs           | no        |           | Comma separated list of objects to apply the privileges on |
| .state          | no        | present   | Whether to `GRANT` or `REVOKE` the privileges |
| remote_accesses | no        |           | List of remote accesses the user is allowed to connect; Each item has following parameters: |
| .databases      | yes       |           | Comma separated list of allowed databases |
| .host           | yes       |           | IP address (with mask!) or host name |


### PostgreSQL configuration

`pg_config_*` variables correspond to PostgreSQL configuration parameters
(without `pg_config_` prefix). See [PostgreSQL
documentation](https://www.postgresql.org/docs/current/static/runtime-config.html)
for details.

`pg` role distinguish between "restart-needed" and "reload-only" option so
there is no need to worry about useless database server restarts.


#### Basic configuration

| Variable                      | Required  | Default       | Description |
| ----------------------------- | --------- | ------------- | ----------- |
| pg_config_listen_addresses    | no        | localhost     | Bind address (use `*` for listen on all interfaces) |
| pg_config_password_encryption | no        | scram-sha-256 | Password hashing method (use `md5` for compatibility with old clients) |


#### Logging parameters

| Variable                              | Required  | Default               | Description |
| ------------------------------------- | --------- | --------------------- | ----------- |
| pg_config_log_temp_files              | no        | 2MB                   | Log temporary files larger than this size |
| pg_config_log_min_duration_statement  | no        | 500                   | Log statements longer than this duration in ms |
| pg_config_log_autovacuum_min_duration | no        | 250                   | Log autovacuum process longer than this duration in ms |
| pg_config_track_io_timing             | no        | on                    | Issue IO timing; **beware** on significant overhead on some platforms! |
| pg_config_log_line_prefix             | no        | "%m [%p] %q%h %u@%d " | Log format |


#### Tuning parameters

| Variable                          | Required  | Default   | Description |
| --------------------------------- | --------- | --------- | ----------- |
| pg_config_shared_buffers          | no        | 128MB     | Shared buffers size (recommended to start with 25 % of dedicated RAM, but not more than 32GB) |
| pg_config_max_connections         | no        | 100       | Number of available connections (2 times number of CPU + effective number of disks; should not be less than 15) |
| pg_config_work_mem                | no        | 4MB       | Default working memory size for one job ((dedicated RAM size - `shared_buffers`)/(8 * `max_connections`)) |
| pg_config_maintenance_work_mem    | no        | 64MB      | Default working memory size for maintenance jobs (can be several times `work_mem`) |
| pg_config_effective_cache_size    | no        | 4GB       | Effective cache size (information for planner, 50 % of dedicated RAM) |


#### WAL archiving

| Variable                  | Required  | Default   | Description |
| ------------------------- | --------- | --------- | ----------- |
| pg_config_archive_mode    | no        | on        | Enable archive mode |
| pg_config_archive_command | no        | /bin/true | Command used for archiving |


#### Replication

| Variable                      | Required  | Default       | Description |
| ----------------------------- | --------- | ------------- | ----------- |
| pg_config_wal_level           | no        | replica       | Amount of information in write ahead log |
| pg_config_synchronous_commit  | no        | remote_write  | Whether transaction commit waits for WAL disk writes |
| pg_config_max_wal_senders     | no        | 5             | Maximum number of simultaneously running WAL sender processes (slightly greater than number of standby servers) |

Following variables must match on *master* and all *standby* nodes. Required
variables are needed only if replication is enabled.

| Variable                      | Required  | Default                       | Description |
| ----------------------------- | --------- | ----------------------------- | ----------- |
| pg_replication_enabled        | no        | false                         | Enable PostgreSQL streaming replication |
| pg_replication_user           | no        | replicator                    | Name of the PG user used for replication |
| pg_replication_password       | yes       |                               | Password for the replication |
| pg_replication_slot           | no        | `inventory_hostname_short`    | Name of the slot for replication; It should contain only alphanumerical characters and must be unique for each *standby* node |
| pg_replication_master         | yes       |                               | Inventory hostname of the *master* node |
| pg_replication_standby_hosts  | yes       |                               | IP addresses (including netmask) of all standby nodes; Used in `pg_hba.conf` |


**Starting the replication**

The start of the replication is automated however, as it is only one-time and
*dangerous* operation for a *standby* nodes, special variables must be defined:

| Variable                      | Required  | Default   | Description |
| ----------------------------- | --------- | --------- | ----------- |
| pg_replication_start          | no        | false     | Execute tasks to start a replication |
| yes_i_know_what_i_am_doing    | no        | false     | Required if you know what you are doing; This parameter is set in `vars` and so it must be set on a command line as an extra variable to take precedence over the default value |

Tasks to start the replication includes:
- *Drop* (archive) PG cluster `main` on *standby* nodes
- Stop Postgres on *standby* nodes
- Run `pg_basebackup` on standby nodes

If you want to add extra *standby* node to an existing replicated
infrastructure, you should *limit* `ansible-playbook` run on a *master* and
your new *standby*. If you run start replication tasks on an existing
*standby*, Ansible will *drop* current cluster and restart the replication
over (which is not desired in most cases).


## Examples

Tune database settings:

```yaml
pg_config_shared_buffers: '1GB'
pg_config_max_connections: '24'
pg_config_work_mem: '16MB'
pg_config_maintenance_work_mem: '128MB'
pg_config_effective_cache_size: '2GB'
```


Configure some databases:

```yaml
pg_databases:
  # database for local system user
  hammond:

  # database for local application
  app-tauri:
    password: 'St4rG4t3C0mm4nd'

  # database for remote application with custom user name
  app-chulak:
    user: 'tealc'
    password: 'B1gJ4ff4'
    remote_hosts:
      - 192.0.2.0/24
      - 2001:db8:dead:beef/64
```


Configure some extra users:

```yaml
pg_users:
  # user `oneill` with remote (all) access to `app-tauri` db
  # and connect privilege to `app-chulak`
  oneill:
    password: '1runTh3SGcmmd'
    privileges:
      - database: 'app-chulak'
        privs: 'CONNECT'
      - database: 'app-tauri'
        privs: 'ALL'
      - database: 'app-tauri'
        type: table
        privs: 'ALL'
        objs: 'ALL_IN_SCHEMA'
      - database: 'app-tauri'
        type: sequence
        privs: 'ALL'
        objs: 'ALL_IN_SCHEMA'
    remote_accesses:
      - databases: 'app-tauri,app-chulak'
        host: '192.0.2.0/24'
      - databases: 'app-tauri,app-chulak'
        host: '2001:db8:dead:beef/64'
```


**Configure replication**

It is recommended to define an inventory group for *master* and *standby* nodes
and set common parameters in `group_vars`.

`inventory.ini`:

```ini
[sgc-db]
tauri.sgc.org
hala.sgc.org
```

`group_vars/sgc-db.yml`:

```yaml
pg_replication_enabled: true
pg_replication_password: 'fR0MR33seTo5th'
pg_replication_master: 'tauri.sgc.org'
pg_replication_standby_hosts:
  - '{{ hostvars["hala.sgc.org"].ipv6_address }}/128'
```

`ansible-playbook` command to run start replication tasks (*run only once*):

```sh
ansible-playbook -C -e pg_replication_start=true -e yes_i_know_what_i_am_doing=true playbook.yml
```

To add another *standby* node to this setup, extend the inventory group and run
`ansible-playbook` command *limited to the new standby node*.

`inventory.ini`:

```ini
[sgc-db]
tauri.sgc.org
hala.sgc.org
orilla.sgc.org
```

`pg_replication_standby_hosts` in `group_vars/sgc-db.yml`:

```yaml
pg_replication_standby_hosts:
  - '{{ hostvars["hala.sgc.org"].ipv6_address }}/128'
  - '{{ hostvars["orilla.sgc.org"].ipv6_address }}/128'
```

Command to start the replication on second *standby*:

```sh
ansible-playbook -C -l tauri.sgc.org:orilla.sgc.org -e pg_replication_start=true -e yes_i_know_what_i_am_doing=true playbook.yml
```
