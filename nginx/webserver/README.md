# Role `nginx/webserver`

Install and configure *nginx* webserver with Let's Encrypt support.


## Variables

| Variable                 | Required | Default              | Description                                                                                                    |
|--------------------------|----------|----------------------|----------------------------------------------------------------------------------------------------------------|
| admin_mails              | yes      |                      | List of admin e-mails (first will be used for certificate registration)                                        |
| certs_restart_commands   | no       |                      | List of commands to run after certificates issue/renew                                                         |
| upgrade                  | no       | false                | Upgrade `simp_le` git repository                                                                               |
|                          |          |                      |                                                                                                                |
| default_vhost            | no       |                      |                                                                                                                |
| .name                    | no       | default              | Name that serve as an identifier (for directory name, etc.)                                                    |
| .server_name             | no       | `inventory_hostname` | Server name for nginx vhost                                                                                    |
| .aliases                 | no       |                      | List of vhost aliases                                                                                          |
|                          |          |                      |                                                                                                                |
| .https_enabled           | no       | true                 | Enable *https* vhost                                                                                           |
| .https_redirect          | no       | true                 | *https only* mode for vhost, redirect http to https with *hsts* header                                         |
| .https_letsencrypt       | no       | true                 | Use *letsencrypt* generated certificate and key (from `acme` role)                                             |
| .https_cert              | no       | snakeoil.crt         | Path to a TLS certificate/fullchain (used only if `https_letsencrypt` is `false`)                              |
| .https_key               | no       | snakeoil.key         | Path to a TLS key (used only if `https_letsencrypt` is `false`)                                                |
| .enable_tls13            | no       | true                 | Enable TLSv1.3 on top of v1.2; TLSv1.2 only would be used otherway (useful on systems without TLSv1.3 support) |
|                          |          |                      |                                                                                                                |
| nginx_default_page       | no       |                      | Parameters for default html page                                                                               |
| .show_admin_footer       | no       | true                 | Display admin contact in footer                                                                                |
| .dark_theme              | no       | false                | Generate dark-themed style                                                                                     |
|                          |          |                      |                                                                                                                |
| nginx_status_enabled     | no       | false                | Enable nginx status virtual host                                                                               |
| nginx_status_vhost       | no       |                      |                                                                                                                |
| .name                    | no       | status               | Name that serve as an identifier (for vhost name, etc.)                                                        |
| .server_name             | no       | localhost            | Server name for nginx vhost                                                                                    |
| .port                    | no       | 8100                 | Listen port for nginx status vhost                                                                             |
|                          |          |                      |                                                                                                                |
| nginx_worker_processes   | no       | auto                 | Number of nginx worker processes; Default value `auto` lets the nginx autodetect it by the number of CPU cores |
| nginx_worker_connections | no       | 768                  | Maximum number of opened connections by a worker process; Default value corresponds with Debian default        |
|                          |          |                      |                                                                                                                |
| web_tls_ciphers          | no       | see below            | List of TLS (strong) cipher suites for `tls_1-2` snippet                                                       |


Default list for TLS ciphers:

```yaml
web_tls_ciphers:
  - ECDHE-ECDSA-AES128-GCM-SHA256
  - ECDHE-RSA-AES128-GCM-SHA256
  - ECDHE-ECDSA-AES256-GCM-SHA384
  - ECDHE-RSA-AES256-GCM-SHA384
  - ECDHE-ECDSA-AES128-CCM8
  - ECDHE-ECDSA-AES256-CCM8
  - ECDHE-ECDSA-CHACHA20-POLY1305
  - ECDHE-RSA-CHACHA20-POLY1305
  - DHE-RSA-AES128-GCM-SHA256
  - DHE-RSA-AES256-GCM-SHA384
  - DHE-RSA-AES128-CCM8
  - DHE-RSA-AES256-CCM8
  - DHE-RSA-CHACHA20-POLY1305
```


## Examples

Enable nginx status vhost (on URL `http://localhost:8100/nginx_status`):

```yaml
nginx_status_enabled: true
```
