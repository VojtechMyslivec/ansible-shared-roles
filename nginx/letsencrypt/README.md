# Role `nginx/letsencrypt`

This is part of `simp_le-wrapper` tool to issue given certificate.

It is intended to use this role with `include_role` statement.


## Description

This include will prepare directory for certificates, create cron job for
renewal and issue the cert if the configuration changed.

Following files will be generates:
- *key*: `/var/lib/letsencrypt/simp_le/_NAME_/key.pem`
- *cert*: `/var/lib/letsencrypt/simp_le/_NAME_/fullchain.pem`


## Variables

| Variable       | Required | Default | Description                                                               |
|----------------|----------|---------|---------------------------------------------------------------------------|
| admin_mails    | yes      |         | List of admin e-mails (first will be used for certificate registration)   |
|                |          |         |                                                                           |
| certificate    | yes      |         | Dictionary of the following keys                                          |
| .name          | yes      |         | *Name* of the certificate used to identify it (`_NAME_` in the path etc.) |
| .server_name   | yes      |         | "ServerName" (*CommonName*) in the certificate                            |
| .aliases       | no       |         | List of aliases in the certificate                                        |
|                |          |         |                                                                           |
| le_force_renew | no       | false   | Force renew the certificates                                              |


## Examples

Certificate for default page (used in `nginx` role):

```yaml
- name: Issue certificate for default vhost
  vars:
    certificate: '{{ default_vhost }}'
  include_role: 'nginx/letsencrypt'
  tags:
    - letsencrypt
```
