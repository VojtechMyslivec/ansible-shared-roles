# Role `swapfile`

Prepare a file and configure it as a permanent swap for a system.


## Description

This role is intended to be deployed on a host to configure swap file for
Linux operating system.

Swap file must be located on file system which supports it (i.e. `ext4` or
similar).

Changing a size of a once-configured swap file is not supported at the moment.


## Variables

| Variable          | Required  | Default     | Description |
| ----------------- | --------- | ----------- | ----------- |
| swapfile_size     | no        | 1024        | Size of a swap file in megabytes |
| swapfile_path     | no        | /swapfile0  | Path including file name of the swap file |
| swapfile_priority | no        | 50          | Swap priority for the swap file (mount option; useful for systems with more swap devices) |
