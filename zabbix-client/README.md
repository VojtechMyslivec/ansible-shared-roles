# Role `zabbix-client`

Install and configure Zabbix agent on a server.


## Description

The role will configure host on the Zabbix server if `zabbix_url` variable is
defined. Username and password is used to access the server API.

The API will be accessed from *localhost* and so it is needed to be accessible,
trusted (https) and to have *python* `zabbix-api` installed.

Install `zabbix-api` from *PyPI*:

```
pip2 install zabbix-api
```


## Variables

| Variable                  | Required  | Default               | Description |
| ------------------------- | --------  | --------------------- | ----------- |
| zabbix_client_name        | no        | `inventory_hostname`  | Name of the Zabbix agent instance |
| zabbix_client_listen      | no        |                       | List of comma delimited IP addresses that the agent listen on |
|                           |           |                       |  |
| zabbix_version            | no        |                       | Install *zabbix-agent* in specified version; The role will install distribution default if not specified |
|                           |           |                       |  |
| zabbix_server             | -         |                       | Hostname or IP address of the Zabbix server (or proxy); Required if `zabbix_server_active` is not defined |
| zabbix_server_active      | -         |                       | Hostname or IP address of the Zabbix active server (or proxy); Required if `zabbix_server` is not defined |
|                           |           |                       |  |
| zabbix_url                | no        |                       | Zabbix (API) URL to configure the host on the Zabbix server; not used if not configured |
| zabbix_user               | -         |                       | Username used for Zabbix server API; required if `zabbix_url` is defined |
| zabbix_password           | -         |                       | Password used for Zabbix server API; required if `zabbix_url` is defined |
| zabbix_host_visible_name  | no        | `zabbix_client_name`  | Host visible name in zabbix server |
| zabbix_host_use_ip        | no        | false                 | Use IP address or DNS name for Zabbix server connection |
| zabbix_proxy_name         | no        |                       | The name of the Zabbix Proxy to be used (compatible with zabbix-proxy role) |
| zabbix_host_groups        | yes       |                       | List of host groups the host is part of; possible groups depend on the server configuration |
| zabbix_host_groups        | no        |                       | List of host groups the host is part of; possible groups depend on the server configuration |
| zabbix_host_macros        | no        |                       | List of zabbix macros to be set; each item has following parameters: |
| .name                     | yes       |                       | Host macro name (without `{$...}` symbols) |
| .value                    | yes       |                       | Host macro value |


## Examples

Set `zabbix_server` in *group_vars*:

```yaml
zabbix_server: 'zabbix.example.org'
```
