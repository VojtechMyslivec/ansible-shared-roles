#!/bin/bash
# The script will print the count of failed systemd services
# Suitable to monitor via zabbix

set -o pipefail

systemctl list-units --failed --no-legend \
    | wc -l
