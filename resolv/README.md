# `resolv` role

Configure `/etc/resolv.conf`. By default, it uses
[odvr.nic.cz](https://www.nic.cz/odvr/) as list of nameservers.


## Variables

| Variable              | Required | Default      | Description             |
|-----------------------|----------|--------------|-------------------------|
| resolv_domain         | no       |              | Domain name of the host |
| resolv_search_domains | no       |              | List of search domains  |
| resolv_nameservers    | no       | see examples | List of nameservers     |


## Examples

Default list of nameservers:

```yaml
resolv_nameservers:
  - '2001:148f:fffe::1'
  - '2001:148f:ffff::1'
  - '193.17.47.1'
  - '185.43.135.1'
```


An example configuration:

```yaml
resolv_domain: 'example.org'
resolv_search_domains:
  - 'example.org'
  - 'example.dev
```
