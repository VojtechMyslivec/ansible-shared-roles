# Role `ansible`

Role to create deploy and connect the host to the Ansible


## Variables

| Variable         | Required | Default          | Description                                 |
|------------------|----------|------------------|---------------------------------------------|
| ansible_home     | no       | /var/lib/ansible | Home directory of the deploy user           |
| ansible_groups   | no       |                  | List of secondary groups of the deploy user |
| ansible_ssh_keys | no       |                  | List of authorized ssh key                  |


## Examples

Define ssh keys and system groups for ansible.

```yaml
ansible_groups:
  - 'ssh'

ansible_ssh_keys:
  - '{{ lookup("file", "users/ansible_john.ssh") }}'
  - '{{ lookup("file", "users/ansible_doe.ssh") }}'
```
