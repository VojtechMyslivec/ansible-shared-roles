# Role `redis`

Install and configure Redis server.


## Description

Role will configure Redis server to listen on Unix socket `redis_socket_path`
and optionally to bind to *localhost* on TCP port `redis_port`.

To enable access to Unix socket, just add desired system user to `redis` group.
All permissions are limited so this system group allows only read/write to the
socket and nothing more.


## Variables

| Variable                     | Required | Default                      | Description                                                              |
|------------------------------|----------|------------------------------|--------------------------------------------------------------------------|
| redis_install_from_backports | no       | false                        | Install `redis-server` from backports repositories (useful on Debian)    |
|                              |          |                              |                                                                          |
| redis_password               | yes      |                              | Configure Redis *auth* password                                          |
|                              |          |                              |                                                                          |
| redis_enable_tcp             | no       | false                        | Listen on TCP port                                                       |
| redis_localhost_only         | no       | true                         | Listen on *localhost* interface only (used only when `redis_enable_tcp`) |
| redis_port                   | no       | 6379                         | Listening TCP port (if `redis_enable_tcp` is true)                       |
| redis_socket_path            | no       | /run/redis/redis-server.sock | Path to listening Unix socket                                            |
|                              |          |                              |                                                                          |
| redis_maxmemory              | no       |                              | Redis maximum memory limit in bytes (with units accepted)                |
| redis_maxmemory_policy       | no       | noeviction                   | Redis *MAXMEMORY POLICY*                                                 |
|                              |          |                              |                                                                          |
| redis_configure_sysctl       | no       | true                         | Configure needed Linux kernel parameters                                 |

For available `redis_maxmemory_policy`,  see [Redis documentation](https://redis.io/topics/config) for available policies.


## Examples

Install Redis server with *auth* required

```yaml
redis_password: 'TheEntropyIs128b'
```


Listen on standard TCP port 6379 (localhost only):

```yaml
redis_password: '...'
redis_enable_tcp: true
```


Listen on all interfaces on standard TCP port:

```yaml
redis_password: '...'
redis_enable_tcp: true
redis_localhost_only: false
```


Limit Redis memory to 2 GB and set *least recently used* policy on volatile
(with TTL set) keys:

```yaml
redis_maxmemory: '2gb'
redis_maxmemory_policy: 'volatile-lru'
```
