#!/bin/bash
# This is just a rsync wrapper to ignore 'Partial transfer due to vanished
# source files' return code which should not be treated as an error
#
# Inspired by script from official repository:
#   https://git.samba.org/?p=rsync.git;a=blob;f=support/rsync-no-vanished

IGNOREEXIT=24

rsync "${@}"
ret=$?

if [[ $ret == "$IGNOREEXIT" ]]; then
    ret=0
fi

exit $ret
