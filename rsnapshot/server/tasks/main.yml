---
# Ansible role
#
# Prepare host for rsnapshot backups with btrfs snapshots

- name: Include common variables
  include_vars: '../../rsnapshot/common/vars.yml'
  tags:
    - always


- block:
  - name: Install required packages
    apt:
      name: '{{ rsnapshot_server_packages }}'
      state: present

  - name: Install support for btrfs
    apt:
      name: '{{ rsnapshot_btrfs_server_packages }}'
      state: present
    when: rsnapshot_btrfs_backend

  tags:
    - apt


- name: Generate ssh key for backup user
  user:
    name: 'root'
    generate_ssh_key: yes
    ssh_key_type: 'ed25519'
    ssh_key_file: '.ssh/id_ed25519'
  tags:
    - ssh_fingerprints


- name: Create desired directories
  file:
    path: '{{ item.path }}'
    state: directory
    owner: 'root'
    group: 'root'
    mode: '{{ item.mode|default("0755") }}'
  loop:
    - path: '{{ rsnapshot_backup_dir }}'
      mode: '0750'
    - path: '{{ rsnapshot_log_dir }}'
      mode: '0750'
    - path: '{{ rsnapshot_conf_dir }}'
    - path: '/opt/scripts'

- name: Check backup root is on btrfs filesystem
  command: >-
    findmnt --type btrfs --target '{{ rsnapshot_backup_dir }}'
  changed_when: false
  when: rsnapshot_btrfs_backend


- name: Configure logrotate
  template:
    src: 'logrotate.conf.j2'
    dest: '/etc/logrotate.d/rsnapshot'
    owner: 'root'
    group: 'root'
    mode: '0644'

- name: Deploy helper scripts
  copy:
    src: '{{ item }}'
    dest: '/opt/scripts/{{ item }}'
    owner: 'root'
    group: 'root'
    mode: '0755'
  loop:
    - 'rsync-ignore-vanished.sh'

- name: Deploy btrfs rsnapshot scripts
  template:
    src: '{{ item }}.j2'
    dest: '/opt/scripts/{{ item }}'
    owner: 'root'
    group: 'root'
    mode: '0755'
  loop:
    - 'rsnapshot_cp-al.sh'
    - 'rsnapshot_rm-rf.sh'


- name: Configure backup of unmanaged hosts
  include_role:
    name: rsnapshot/instance
  loop: '{{ rsnapshot_unmanaged_backups }}'
  loop_control:
    loop_var: 'backup_target'
  tags:
    - instance

...
