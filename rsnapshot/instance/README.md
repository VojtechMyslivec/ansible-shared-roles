# Role `rsnapshot/instance`

Configure rsnapshot backup on prepared backup server.


## Description

Configure backup instance on rsnapshot server. This role is used by *client*
role and also by *server* role to configure backups of so-called *unmanaged*
clients. Backup instance is deployed with *btrfs snapshots* support optionally.

This role must run on `backup_server` or be delegated to it.


## Variables

| Variable                | Required | Default | Description                                   |
|-------------------------|----------|---------|-----------------------------------------------|
| rsnapshot_btrfs_backend | no       | true    | Deploy backup instance with *btrfs snapshots* |
| backup_server           | yes      |         | Inventory hostname of backup server           |
| backup_target           | yes      |         | Dictionary with backup parameters             |

Dictionary `backup_target` has following parameters:

| Variable        | Required | Default             | Description                                                                                                           |
|-----------------|----------|---------------------|-----------------------------------------------------------------------------------------------------------------------|
| name            | yes      |                     | Unique identification of the backup; It should consist of `a-zA-Z0-9-_` characters only                               |
| hostname        | yes      |                     | Remote host to backup (ssh server)                                                                                    |
| user            | yes      |                     | ssh user on remote host                                                                                               |
| ssh_args        | no       |                     | Extra parameters for ssh connection                                                                                   |
| rsync_remote    | no       | sudo /usr/bin/rsync | `rsync` command on the remote host                                                                                    |
| snapshots       | no       |                     | A dict with configured *backup levels*: Value represents how many copies is kept and level is disabled if set to `0`  |
| .hourly         | no       | 24                  | Hourly backups                                                                                                        |
| .daily          | no       | 7                   | Daily backups (before 2 AM after hourly backup)                                                                       |
| .weekly         | no       | 4                   | Weekly backup (before 4 AM at Saturday after hourly backup and with `Persistent` flag)                                |
| .monthly        | no       | 2                   | Monthly backup (before 4 AM at Friday after first Saturday in a month after hourly backup and with `Persistent` flag) |
| hourly_interval | no       |                     | Define interval for hourly backups (in hours); By default, perform hourly backup every hour                           |
| files           | no       | see examples        | List of files (and directories) to backup                                                                             |
| exclude         | no       |                     | List of excluded files (and directories) from the backup                                                              |
| default_exclude | no       | see examples        | List of default excluded files (suitable to combine with host-specific `exclude`)                                     |


## Examples

Default backup parameters:

```yaml
backup_target:
  remote_rsync: 'sudo /usr/bin/rsync'
  snapshots:
    hourly: 24
    daily: 7
    weekly: 4
    monthly: 2
  files:
    - '/'
  exclude: []
  default_exclude:
    - '/dev'
    - '/proc'
    - '/run'
    - '/swapfile0'
    - '/sys'
    - '/var/cache'
    - '/var/lib/mysql'
    - '/var/lib/postgresql'
    - '/var/run'
    - '/var/tmp'
    - '/tmp'
    - 'lost+found'
```


Disable *hourly* backups (must be changed manually afterwards):

```yaml
backup_target:
  snapshots:
    hourly: 0
```


Create hourly backups every 6 hours only (four backups a day):

```yaml
backup_target:
  snapshots:
    hourly: 4
  hourly_interval: 6
```
