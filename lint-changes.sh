#/bin/bash

lint_changes() {
    local array role
    while read -r role; do
        array+=("$role")
    done <<< "$(changed_roles)"

    ansible-lint "${array[@]}"
}


changed_roles() {
    local changed_files
    changed_files=$(
        git diff --name-only origin/main...HEAD --
    )

    local file
    while read -r file; do
        find_role "$file" || {
            echo "warning: Can't find role for '$file'" >&2
        }
    done <<< "$changed_files" \
        | sort -u
}


find_role() {
    local file=$1
    [[ "$file" == '.' ]] && return 1
    if ls "$file"/*/main.yml >& /dev/null; then
        printf '%s\n' "$file"
        return 0
    else
        find_role "$(dirname "$file")"
    fi
}


lint_changes
